import tensorflow as tf
from tensorflow.python.framework.errors_impl import InvalidArgumentError

from rnntimeseries.input import read_input as input_reader
from rnntimeseries.input.input_data_to_time_seq import InputDataToTimeSeqs
from rnntimeseries.input.transform import input_line

TRAIN_FILES = ["/home/kkurek/PycharmProjects/rnn_time_series/data/train0.csv"]
TIME_STEPS = 5
FEATS_CNT = 5
EPOCHS = 1
BATCH_SIZE = 50
BATCH_SUMMARY_STEP = 10
NEURONS_CNT = 50
TARGET_FEAT_NUM = 0

input_to_time_seq_transformer = InputDataToTimeSeqs(TIME_STEPS, BATCH_SIZE)
dataset = input_reader.read_input(TRAIN_FILES, BATCH_SIZE, 1, lambda line: input_line.convert_to_float(line))
dataset_it = dataset.make_initializable_iterator()

x = tf.placeholder(tf.float32, [None, TIME_STEPS, FEATS_CNT])
y = tf.placeholder(tf.float32, [None, TIME_STEPS, 1])
cell = tf.contrib.rnn.BasicRNNCell(num_units=NEURONS_CNT, activation=tf.nn.relu)
rnn_outputs, states = tf.nn.dynamic_rnn(cell, x, dtype=tf.float32)
stacked_rnn_outputs = tf.reshape(rnn_outputs, [-1, NEURONS_CNT])
stacked_outputs = tf.layers.dense(stacked_rnn_outputs, 1)
outputs = tf.reshape(stacked_outputs, [-1, TIME_STEPS, 1])

loss = tf.reduce_mean(tf.square(outputs - y))
optimizer = tf.train.AdamOptimizer()
training_op = optimizer.minimize(loss)


def train_epoch(sess: tf.Session, dataset_it):
    try:
        batch_num = 0
        while True:
            print("Runing batch %d" % batch_num)
            next_batch = dataset_it.get_next()
            input_seq, target_seq = input_to_time_seq_transformer.read_input(next_batch)
            target_feat = target_seq[:, :, TARGET_FEAT_NUM:TARGET_FEAT_NUM + 1]
            x_values, target_values = sess.run([input_seq, target_feat])
            sess.run(training_op, feed_dict={x: x_values, y: target_values})
            if batch_num % BATCH_SUMMARY_STEP == 0:
                mse = loss.eval(feed_dict={x: x_values, y: target_values})
                print("In batch %d, loss=%d" % (batch_num, mse))
            batch_num += 1
    except (tf.errors.OutOfRangeError, InvalidArgumentError):
        mse = loss.eval(feed_dict={x: x_values, y: target_values})
        print("In batch %d, loss=%d" % (batch_num, mse))


global_init = tf.global_variables_initializer()
with tf.Session() as sess:
    sess.run(global_init)
    for i in range(EPOCHS):
        sess.run(dataset_it.initializer)
        print("Training epoch %d" % i)
        train_epoch(sess, dataset_it)
