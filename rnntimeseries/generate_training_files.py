import numpy as np
import pandas as pd
import os

OUT_DIR = "/home/kkurek/PycharmProjects/rnn_time_series/data"
TRAIN_FILES_CNT = 1
SAMPLES_CNT = 1440
FEATS_CNT = 5
FEAT_NAME_FORMAT = "feat%d"


def generate_train_files():
    if not os.path.isdir(OUT_DIR):
        os.makedirs(OUT_DIR)

    for i in range(TRAIN_FILES_CNT):
        generate_single_file(i)


def generate_single_file(file_num: int):
    x = np.random.normal(100, 20, (SAMPLES_CNT, FEATS_CNT))
    df = pd.DataFrame(x, columns=[FEAT_NAME_FORMAT % i for i in range(FEATS_CNT)])
    df.to_csv(os.path.join(OUT_DIR, "train%d.csv" % file_num), index=False)


if __name__ == "__main__":
    generate_train_files()
