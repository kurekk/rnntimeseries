import tensorflow as tf


def convert_to_float(line):
    line_splitted = tf.string_split([line], ",")
    str_data = tf.convert_to_tensor(line_splitted.values, dtype=tf.string)
    float_data = tf.string_to_number(str_data, out_type=tf.float32)

    return float_data


def convert_to_float_and_split_feat_labels(line, feat_idx_delim=-1):
    float_data = convert_to_float(line)

    return float_data[:feat_idx_delim], float_data[feat_idx_delim:]
