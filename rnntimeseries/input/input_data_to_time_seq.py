import numpy as np

import tensorflow as tf


class InputDataToTimeSeqs:
    def __init__(self, time_steps: int, batch_size: int):
        self.__time_steps = time_steps
        self.__batch_size = batch_size

    def read_input(self, next_batch):
        begin_idx = 0
        end_idx = self.__calculate_end_idx(begin_idx)
        seqs_list = list()
        while end_idx <= self.__batch_size:
            seqs_list.append(next_batch[begin_idx:end_idx, :])
            begin_idx += 1
            end_idx = begin_idx + self.__time_steps
        time_seqs = tf.convert_to_tensor(seqs_list)

        return time_seqs[:-1, :], time_seqs[1:, :]

    def __calculate_end_idx(self, begin_idx: int) -> int:
        return begin_idx + self.__time_steps
