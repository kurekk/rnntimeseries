import tensorflow as tf

from unittest import TestCase

from rnntimeseries.input.input_data_to_time_seq import InputDataToTimeSeqs
from rnntimeseries.input.read_input import read_input
from rnntimeseries.input.transform import input_line


class TestInputDataToTimeSeq(TestCase):
    BATCH_SIZE = 50

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.train_files = ["/home/kkurek/PycharmProjects/rnn_time_series/data/train0.csv"]

    def test_transform_input_data_to_time_seq(self):
        input_to_time_seq_transformer = InputDataToTimeSeqs(5, self.BATCH_SIZE)
        dataset = read_input(self.train_files, self.BATCH_SIZE, 1, lambda line: input_line.convert_to_float(line))
        dataset_it = dataset.make_initializable_iterator()
        global_init = tf.global_variables_initializer()
        with tf.Session() as sess:
            sess.run([global_init, dataset_it.initializer])
            try:
                while True:
                    next_batch = dataset_it.get_next()
                    input_seq, target_seq = input_to_time_seq_transformer.read_input(next_batch)
                    target_feat_seq = target_seq[:, :, 0:1]
                    input_seq_out, target_seq_out, target_feat_out = sess.run([input_seq, target_seq, target_feat_seq])
            except tf.errors.OutOfRangeError:
                pass
